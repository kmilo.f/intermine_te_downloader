import sys
import platform
import datetime
import csv
from intermine.webservice import Service
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

from common.argumentParser import ArgumentParser as Ap
from common.commonFunctions import CommonFunctions as Cf

from tqdm import tqdm

program_name = 'intermine_downloader'
program_ver = '1.0.0'
system = platform.system()


def get_genomic_data(service_type, user_token):
    """
    Downloading TE data from http://registry.intermine.org/ databases.
    :param service_type:
    :param user_token:
    :return:
    """

    query = ''
    output_file = ''
    organism = ''
    if service_type == 'sc':
        print('Sacharomyces cerevisiae')
        service = Service("https://yeastmine.yeastgenome.org/yeastmine/service")
        print('1 - Long Terminal Repeats')
        print('2 - Retrotransposon')
        print('3 - Transposable Element Gene')
        print('x - Exit.')
        while True:
            try:
                user_input = Cf().check_input()
                if user_input == 'x':
                    print('Terminating.')
                    exit()
                elif user_input == '1':
                    query = service.new_query("LongTerminalRepeat")
                    output_file = 'ltr_sc_'
                elif user_input == '2':
                    query = service.new_query("Retrotransposon")
                    output_file = 'retrotransposon_sc_'
                elif user_input == '3':
                    query = service.new_query("TransposableElementGene")
                    output_file = 'rransposable_element_gene_sc_'
                elif not (1 <= int(user_input) <= 3):
                    raise ValueError
            except ValueError:
                print('Invalid option')
            else:
                break
        organism = 'Sacharomyces cerevisiae'
    elif service_type == 'dr':
        print('flybase - drosophila')
        service = Service("https://www.flymine.org/flymine/service")
        query = service.new_query("TransposableElement")
        output_file = 'te_dr_'
        organism = 'Drosophila'
    elif service_type == 'lm':
        print('Legume - ')
        service = Service("https://mines.legumeinfo.org/legumemine/service")
        print('1 - LTR Retrotransposon')
        print('2 - Non LTR Retrotransposon')
        print('3 - Transposable Element')
        print('4 - Retrotransposon')
        print('5 - TRIM Retrotransposon')
        print('x - Exit.')
        while True:
            try:
                user_input = Cf().check_input()
                if user_input == 'x':
                    print('Terminating.')
                    exit()
                elif user_input == '1':
                    query = service.new_query("LTRRetrotransposon")
                    output_file = 'ltr_legume_'
                elif user_input == '2':
                    query = service.new_query("NonLTRRetrotransposon")
                    output_file = 'nonltr_legume_'
                elif user_input == '3':
                    query = service.new_query("TransposableElement")
                    output_file = 'te_legume_'
                elif user_input == '4':
                    query = service.new_query("Retrotransposon")
                    output_file = 'retro_legume_'
                elif user_input == '5':
                    query = service.new_query("TRIMRetrotransposon")
                    output_file = 'trim_retro_legume_'
                elif not (1 <= int(user_input) <= 5):
                    raise ValueError
            except ValueError:
                print('Invalid option')
            else:
                break
        organism = 'Legume'
    elif service_type == 'repdb':
        print('RepeatDB - ')
        service = Service("http://urgi.versailles.inra.fr/repetdb/service")
        query = service.new_query("Consensus")
        print('1 - Arabidopsis thaliana')
        print('2 - Arabidopsis lyrata')
        print('3 - Arabis alpina')
        print('4 - Blumeria graminis')
        print('5 - Botrytis cinerea')
        print('6 - Brassica rapa')
        print('7 - Capsella rubella')
        print('8 - Colletotrichum higginsianum')
        print('9 - Fragaria vesca')
        print('10 - Magnaporthe oryzae')
        print('11 - Malus domestica')
        print('12 - Melampsora larici-populina')
        print('13 - Microbotryum lychnidis-dioicae')
        print('14 - Prunus persica')
        print('15 - Puccinia graminis')
        print('16 - Pyrus communis')
        print('17 - Sclerotinia sclerotiorum')
        print('18 - Schrenkiella parvula')
        print('19 - Triticum aestivum')
        print('20 - Tuber melanosporum')
        print('21 - Vitis vinifera')
        print('22 - Zea mays')
        print('x - Exit.')
        while True:
            try:
                user_input = Cf().check_input()
                if user_input == '1':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "3702", code="A")
                    output_file = 'tes_a_thaliana'
                    organism = 'Arabidopsis thaliana'
                if user_input == '2':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "81972", code="A")
                    output_file = 'tes_a_lyrata_'
                    organism = 'Arabidopsis lyrata'
                if user_input == '3':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "50452", code="A")
                    output_file = 'tes_a_alpina_'
                    organism = 'Arabis alpina'
                if user_input == '4':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "546991", code="A")
                    output_file = 'tes_b_graminis_'
                    organism = 'Blumeria graminis'
                if user_input == '5':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "332648", code="A")
                    output_file = 'tes_b_cinerea_'
                    organism = 'Botrytis cinerea'
                if user_input == '6':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "3711", code="A")
                    output_file = 'tes_b_rapa_'
                    organism = 'Brassica rapa'
                if user_input == '7':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "81985", code="A")
                    output_file = 'tes_c_rubella_'
                    organism = 'Capsella rubella'
                if user_input == '8':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "759273", code="A")
                    output_file = 'tes_c_higginsianum_'
                    organism = 'Colletotrichum higginsianum'
                if user_input == '9':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "57918", code="A")
                    output_file = 'tes_f_vesca_'
                    organism = 'Fragaria vesca'
                if user_input == '10':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "242507", code="A")
                    output_file = 'tes_m_oryzae_'
                    organism = 'Magnaporthe oryzae'
                if user_input == '11':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "3750", code="A")
                    output_file = 'tes_m_domestica_'
                    organism = 'Malus domestica'
                if user_input == '12':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "747676", code="A")
                    output_file = 'tes_m_larici_populina_'
                    organism = 'Melampsora larici-populin'
                if user_input == '13':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "683840", code="A")
                    output_file = 'tes_m_lychnidis_dioicae_'
                    organism = 'Microbotryum lychnidis-dioicae'
                if user_input == '14':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "3760", code="A")
                    output_file = 'tes_p_persica_'
                    organism = 'Prunus persica'
                if user_input == '15':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "418459", code="A")
                    output_file = 'tes_p_graminis_'
                    organism = 'Puccinia graminis'
                if user_input == '16':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "23211", code="A")
                    output_file = 'tes_p_communis_'
                    organism = 'Pyrus communis'
                if user_input == '17':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "665079", code="A")
                    output_file = 'tes_s_sclerotiorum_'
                    organism = 'Sclerotinia sclerotiorum'
                if user_input == '18':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "98039", code="A")
                    output_file = 'tes_s_parvula_'
                    organism = 'Schrenkiella parvula'
                if user_input == '19':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "4565", code="A")
                    output_file = 'tes_t_aestivum_'
                    organism = 'Triticum aestivum'
                if user_input == '20':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "39416", code="A")
                    output_file = 'tes_t_melanosporum_'
                    organism = 'Tuber melanosporum'
                if user_input == '21':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "29760", code="A")
                    output_file = 'tes_v_vinifera_'
                    organism = 'Vitis vinifera'
                if user_input == '22':
                    query.add_constraint("organism.taxonomyNode.parents.taxonId", "=", "4577", code="A")
                    output_file = 'tes_z_mays_'
                    organism = 'Zea mays'
            except ValueError:
                print('Invalid option')
            else:
                break
    else:
        service = Service("https://www.flymine.org/flymine/service")
        query = service.new_query("TransposableElement")
        output_file = 'te_dr_'

    query.add_view("primaryIdentifier", "sequence.residues", "organism.name")

    with open(output_file + '_te_data.csv', mode='w', newline='') as csv_file:
        tes_writer = csv.writer(csv_file, delimiter='\t')
        tes_writer.writerow(['id', 'secondary_id', 'description', 'organism_name'])
        progress_bar = tqdm(total=len(query))
        progress_bar.set_description(organism)
        with open(output_file + 'data.fasta', mode='w') as fasta_file:
            for row in query.rows():
                # print(row['description'])
                progress_bar.update(1)
                try:
                    description = row['briefDescription']
                except KeyError:
                    try:
                        description = row['description']
                    except KeyError:
                        description = 'NA'

                if row['secondaryIdentifier'] is None:
                    secondary_id = 'NA'
                else:
                    secondary_id = row['secondaryIdentifier']

                record = SeqRecord(seq=Seq(row['sequence.residues']),
                                   id=row['primaryIdentifier'] + ' ' + row['organism.name'] + ' ' + secondary_id,
                                   name=row['organism.name'], description=description)
                SeqIO.write(record, fasta_file, 'fasta')
                tes_writer.writerow(
                    [row['primaryIdentifier'], row['secondaryIdentifier'], row['organism.name'], description])
        progress_bar.close()


def main(args):
    arguments = Ap(prog_version=program_ver, argv=args, program_name=program_name)
    print(arguments.return_arguments())
    start_time = datetime.datetime.now()
    print('\nStarted at: ', start_time)

    get_genomic_data(service_type=arguments.return_arguments().service_type,
                     user_token=arguments.return_arguments().token)


if __name__ == '__main__':
    main(sys.argv[1:])
