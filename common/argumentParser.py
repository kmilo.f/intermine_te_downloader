import argparse
import sys


class ArgumentParser(object):
    def __init__(self, prog_version, argv, program_name):
        """

        :param prog_version:
        :param argv:
        :param program_name:
        """

        self.program_v = prog_version
        self.arguments = argv
        self.ap = argparse.ArgumentParser(description=program_name + "script to Download data from YeastMine"
                                                                     "\n Program version: " + self.program_v)

        self.ap.add_argument('-s', '--service_type', action='store', required=True, default='sc', type=str,
                             help='Intermine service from where to download the data.')
        self.ap.add_argument('-t', '--token', action='store', required=False, type=str,
                             help='Intermine personal token.')
    def return_arguments(self):
        """

        :return: Processed arguments.
        """
        return self.ap.parse_args(self.arguments)
