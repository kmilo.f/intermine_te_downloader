class CommonFunctions(object):
    def __init__(self):
        """
        Set of common function used by the program itself.
        Refer to each function for details
        """
        pass

    @staticmethod
    def check_input():
        """

        Function to check if the input typed it's a valid one or not.

        :return: return input.
        """
        while True:
            try:
                typed_input = input("Please type your input : ")
                if not typed_input:
                    raise TypeError()
                else:
                    print("\n* Using \" %s \" * \n" % typed_input)
            except TypeError:
                print('Please enter a valid input.')
            else:
                break

        return typed_input
