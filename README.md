# intermine TE donwloader.

Script to donwload data from http://intermine.org/.

This script fetch data from Tranposable Elements (and variants) from different intermine databases.
Write them in FASTA format (multi fasta) and a csv of the related data.

This hard encoded in the script, but they can be changed as per necessity. (Probably an argument would be nice, but this suite my purposes)

Key Variables

```python
from intermine.webservice import Service

service = Service("https://www.flymine.org/flymine/service")
query = service.new_query("TransposableElement")

query.add_view("primaryIdentifier", "sequence.residues", "organism.name")

for row in query.rows():
    # row manipulation
    print(row)
```

* `Service` allows to change the desire intermine database
* `Query` specify which element to fetch (Interchangeable and seems to be transversal among databases)
* `...add_view` Specify which data from the element we want to fetch.
* `query.rows()` allows to traverse the obtained results.

## requirements

usual python package/environment manager is suggested. (Pip / conda / miniconda)
* python = 3.3 +
* biopython (conda install -c conda-forge biopython )
* intermine python library (conda install -c bioconda intermine)
* tqdm (conda install -c conda-forge tqdm)


## arguments

* `-s service` from where to download the data
* `-t token` sometimes a intermine database require a token. Create an account on the desire database and get your token.

#### available services (Intermine Datbases)
* sc - Saccharomyces Cerevisiae
* dr - Drosophila
* lm - legume
*  repdb - (various _plants_)
